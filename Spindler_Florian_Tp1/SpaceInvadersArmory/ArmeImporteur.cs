﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvadersArmory
{
    public class ArmeImporteur
    {
        private Dictionary<string, int> dico { get; set; } = new Dictionary<string, int>();
        private List<string> blacklist { get; set; } = new List<string>();

        public void ArmeImposteur()
         {

         }

         public void ReadFile(string path,int min)
         {
            string linelower;
            blacklist.Add("pokemon");
            foreach (string line in File.ReadLines(path))
            {
                linelower = line.ToLower();
                if (!blacklist.Contains(linelower) && linelower.Length >= min)
                {
                    if(dico.ContainsKey(linelower))
                    {
                        dico[linelower] = dico[linelower] + 1;
                    }
                    else
                    {
                        dico.Add(linelower, 1);
                    }
                }
            }
         }
        public void ImportWeapon()
        {
            EWeaponType type = EWeaponType.Direct;
            int i;
            Random rand = new Random();
            foreach (var arme in dico)
            {
                i = rand.Next(0,3);
                switch(i)
                {
                    case 0:
                        type = EWeaponType.Direct;
                        break;
                    case 1:
                        type = EWeaponType.Guided;
                        break;
                    case 2:
                        type = EWeaponType.Explosive;
                        break;
                }  
                Armory.CreatBlueprint(arme.Key,type,(arme.Key.Length+arme.Value)/2, arme.Key.Length + arme.Value);
            }
        }
    }
}
